/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cube.class.hpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 11:20:47 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/31 12:11:26 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUBE_CLASS_HPP
# define CUBE_CLASS_HPP

# include <iostream>
# include <FL/Fl.H>
# include <FL/Fl_Box.H>
# include <FL/fl_draw.H>

class Cube: public Fl_Box
{
	public:
		//start canonical form
		Cube(void);
		Cube(int const in_x, int const in_y, Fl_Color const in_col);
		Cube(Cube const & src);
		~Cube(void);
		void draw();

		Cube	&operator=(Cube const &rhs);
		//end canonical form

		int	getX(void) const;
		int	getY(void) const;
		void setXY(int const in_x, int const in_y);
		void setCol(Fl_Color in_col);
		Fl_Color getCol(void) const;
	private:
		int	x = 0;
		int	y = 0;
		Fl_Color col = FL_WHITE;
};

#endif
