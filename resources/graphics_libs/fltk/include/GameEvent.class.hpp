/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   GameEvent.class.hpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 11:20:47 by cnolte            #+#    #+#             */
/*   Updated: 2018/06/11 11:49:11 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GAMEEVENT_CLASS_HPP
# define GAMEEVENT_CLASS_HPP

# include <iostream>
# include <FL/Fl_Window.H>
# include "Cube.class.hpp"

class GameEvent
{
	public:
		//start canonical form
		GameEvent(void);
		GameEvent(int const n);
		GameEvent(GameEvent const & src);
		~GameEvent(void);

		GameEvent	&operator=(GameEvent const &rhs);
		//end canonical form

		void	gameLoop(void);
		char	getLastKeyPressed(void);
	private:
		char lastKeyPressed;
		Cube *b;
};

std::ostream	&operator<<(std::ostream &out, GameEvent const &value);

#endif
