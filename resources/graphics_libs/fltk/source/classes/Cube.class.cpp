/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cube.class.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 11:26:34 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/29 16:28:12 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Cube.class.hpp"

//start canonical form
Cube::Cube(void) : x(0), y(0), col(FL_WHITE), Fl_Box(0,1,1,0)
{
	std::cout << "CUBE Default Constructor Called" << std::endl;
	Fl_Window w(500,500,"MyWindow");
	return ;
}

Cube::Cube(int const in_x, int const in_y, Fl_Color const in_col) : x(in_x), y(in_y), col(in_col), Fl_Box(0,1,1,0)
{
	std::cout << "CUBE Parametric Constructor called" << std::endl;

	return ;
}

Cube::Cube(Cube const & src): Fl_Box(0,1,1,0)
{
	std::cout << "CUBE Copy constructor called" << std::endl;
	*this = src;

	return ;
}

Cube::~Cube(void)
{
	std::cout << "CUBE Destructor called" << std::endl;
	return ;
}

Cube	&Cube::operator=(Cube const &rhs)
{
	std::cout << "CUBE Assignment operator called" << std::endl;
	if (this != &rhs)
	{
		this->x = rhs.x;
		this->y = rhs.y;
		this->col = rhs.col;
	}
	return (*this);
}
//end canonical form

void Cube::draw(void)
{
	fl_rectf(x, y, 50, 50, col);
}

int	Cube::getX(void) const
{
	return (this->x);
}

int	Cube::getY(void) const
{
	return (this->y);
}

void	Cube::setXY(int const in_x, int const in_y)
{
	this->x = in_x;
	this->y = in_y;
}

void	Cube::setCol(Fl_Color in_col)
{
	this->col = in_col;
}

Fl_Color	Cube::getCol(void) const
{
	return (this->col);
}
