/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   GameEvent.class.cpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 11:26:34 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/31 16:24:53 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "GameEvent.class.hpp"

//start canonical form
GameEvent::GameEvent(void) : lastKeyPressed('l')
{
	std::cout << "GAME Default Constructor Called" << std::endl;
	Fl_Window w(500,500,"MyWindow");
	Cube a(40,50, FL_GREEN);
	this->b = &a;
	w.show();
	gameLoop();
	return ;
}

GameEvent::GameEvent(GameEvent const & src)
{
	std::cout << "GAME Copy constructor called" << std::endl;
	*this = src;

	return ;
}

GameEvent::~GameEvent(void)
{
	std::cout << "GAME Destructor called" << std::endl;
	return ;
}

GameEvent	&GameEvent::operator=(GameEvent const &rhs)
{
	std::cout << "GAME Assignment operator called" << std::endl;
	// if (this != &rhs)
	// 	this->_foo = rhs.getfoo();
	return (*this);
}
//end canonical form

char		GameEvent::getLastKeyPressed(void)
{
	return (this->lastKeyPressed);
}

void		GameEvent::gameLoop(void)
{
	while (Fl::wait())
	{
		if (Fl::get_key(Fl::event_key()) == 1)
		{
			switch (Fl::event_key())
			{
				case FL_Left:
					this->b->setXY(b->getX() - 15, b->getY());
					this->lastKeyPressed = 'l';
					break ;
				case FL_Right:
					this->b->setXY(b->getX() + 15, b->getY());
					this->lastKeyPressed = 'r';
					break ;
				case FL_Up:
					this->b->setXY(b->getX(), b->getY() - 15);
					this->lastKeyPressed = 'u';
					break ;
				case FL_Down:
					this->b->setXY(b->getX(), b->getY() + 15);
					this->lastKeyPressed = 'd';
					break ;
				default:
					this->lastKeyPressed = '\0';
					break ;
			}
		}
		Fl::redraw();
		std::cout << getLastKeyPressed() << std::endl;
	}
}
