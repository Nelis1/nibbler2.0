#ifndef SAMPLE_CLASS_HPP
# define SAMPLE_CLASS_HPP

# include <iostream>

class Sample
{
	public:
		//start canonical form
		Sample(void);
		Sample(Sample const & src);
		~Sample(void);

		Sample	&operator=(Sample const &rhs);
		//end canonical form
	private:
};

#endif
