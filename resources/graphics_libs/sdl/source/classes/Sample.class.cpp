#include <iostream>
#include "Sample.class.hpp"

//start canonical form
Sample:: Sample(void)
{
	std::cout << "Default Constructor Called" << std::endl;
	return ;
}

Sample::Sample(Sample const &src)
{
	std::cout << "Copy constructor called" << std::endl;
	*this = src;

	return ;
}

Sample::~Sample(void)
{
	std::cout << "Destructor called" << std::endl;
	return ;
}

Sample	&Sample::operator=(Sample const &rhs)
{
	std::cout << "Assignment operator called" << std::endl;
	// if (this != &rhs)
	// 	this->_foo = rhs.getfoo();
	return (*this);
}
//end canonical form
