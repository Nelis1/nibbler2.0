#include "sdlheader.hpp"

SDL_Renderer	*init(std::string title)
{
	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_Window *win = SDL_CreateWindow(title.c_str(), 100, 100, 640, 480, SDL_WINDOW_SHOWN);
	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);
	SDL_RenderPresent(ren);
	return (ren);
}

int	main(void)
{
	Sample	inst;
	SDL_Renderer	*ren = init("My World");
	SDL_Event		event;
	int				quit;
	int				x;
	int				y;

	quit = 0;
	while (!quit)
    {
        /* Check for new events */
        while(SDL_PollEvent(&event))
        {
			SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
			SDL_RenderClear(ren);
			SDL_Rect srcrect;
			SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
			srcrect.x = 50;
			srcrect.y = 50;
			srcrect.w = 32;
			srcrect.h = 32;
			SDL_RenderFillRect(ren, &srcrect);
			SDL_RenderPresent(ren);
			if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
				SDL_RenderClear(ren);
				SDL_GetMouseState(&x, &y);
				SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
				draw_line(ren, 320, 240, x, y);
				SDL_RenderPresent(ren);
			}
			/* If a quit event has been sent */
			switch (event.type)
			{
				case SDL_QUIT:
					quit = 1;
					break ;
				case SDL_KEYDOWN:
					switch (event.key.keysym.sym)
					{
						case SDLK_ESCAPE:
							quit = 1;
							break ;
						default:
							break ;
					}
					quit = 1;
					break ;
				default:
					break ;
			}
		}
	}
	SDL_Quit();
	return (0);
}
