#include <SFML/Graphics.hpp>

int main()
{
	sf::RenderWindow window(sf::VideoMode(2000, 2000), "SFML works!");
	sf::CircleShape square(80, 4);
	square.setFillColor(sf::Color::Green);
	square.rotate(45);
	square.move(100,50);

	int x = 0;
	while (window.isOpen())
	{
		sf::Event event;
		if ((event.type == event.KeyPressed) && (event.key.code == sf::Keyboard::Up))
		{
			square.move(0,-5);
		}
		if ((event.type == event.KeyPressed) && (event.key.code == sf::Keyboard::Down))
		{
			square.move(0,5);
		}
		if ((event.type == event.KeyPressed) && (event.key.code == sf::Keyboard::Left))
		{
			square.move(-5,0);
		}
		if ((event.type == event.KeyPressed) && (event.key.code == sf::Keyboard::Right))
		{
			square.move(5,0);
		}
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		window.clear();
		window.draw(square);
		window.display();
	}
	return 0;
}